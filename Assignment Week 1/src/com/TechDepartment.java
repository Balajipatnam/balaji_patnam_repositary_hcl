package com;

public class TechDepartment extends SuperDepartment {
	public String techDepartment()
	{
		return "Tech Department";
	}
	public String getTodaysWork()
	{
		return "Complete coding of module 1";
	}
	public String getWorkDeadline()
	{
		return "Complete by the End Of The Day";
	}
	public String getTechStackInformation()
	{
		return "core java";
	}


}
